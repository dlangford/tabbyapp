//
//  TabbyAppTests.m
//  TabbyAppTests
//
//  Created by Debra Langford on 11/24/12.
//  Copyright (c) 2012 Debra Langford. All rights reserved.
//

#import "TabbyAppTests.h"

@implementation TabbyAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in TabbyAppTests");
}

@end
