//
//  AppDelegate.h
//  TabbyApp
//
//  Created by Debra Langford on 11/24/12.
//  Copyright (c) 2012 Debra Langford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
